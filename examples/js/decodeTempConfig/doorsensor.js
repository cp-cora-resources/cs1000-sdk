"use strict";

function main() {
  // [START proto3_json_serializer_doorsensor]
  function doorsensor() {
    const assert = require("assert");
    const protobuf = require("protobufjs");

    // Load bundle
    const jproto = require("cs1000-protobuf-v1.0.json");
    const rootCs1000 = protobuf.Root.fromJSON(jproto);

    //Decode the encoded temperature configuration message.
    const tempcfg = rootCs1000.lookupType("Temperature.Configuration");
    const buff = Buffer.from(
      "CAASCAgAEB4YASAAEggIARAZGAEgARIICAIQAxgBIAIYACACKDwwATgB",
      "base64"
    );
    const msg = tempcfg.decode(buff);
    console.log(JSON.stringify(msg, null, 2));
  }

  doorsensor();
}

main();
